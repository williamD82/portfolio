import React from "react";
import { Theme } from "../../styles/theme";
import { styled } from "styled-components";
import CardExperience from "../CardExperience";
import { EXPERIENCE } from "../../commons/constants";
import Rotate from "react-reveal/Rotate";
import MyProjects from "../MyProjects";
import Footer from "../Footer";

const Contenair = styled.div`
  overflow: scroll;
  position: initial;
  @media (max-width: 768px) {
    position: static;
    padding: 20px;
  }
`;
const MainDescription = styled.p`
  color: ${Theme.colors.primary.default};
  line-height: 1.5;
  max-width: 35rem;
  margin-top: 1rem;
  padding-top: 7rem;
  padding-bottom: 6rem;
`;
const ContenairExperience = styled.div`
  color: ${Theme.colors.primary.default};
  line-height: 1.5;
  max-width: 35rem;
  margin-top: 1.2rem;
  padding: 2px;
`;
const WordStyled = styled.span`
  color: ${Theme.colors.primary.lighter};
`;
const Title = styled.h3`
  margin-top: 50px;
  color: ${Theme.colors.primary.default};
`;
const MyDescription = () => {
  return (
    <Contenair>
      <MainDescription id="description">
        Je suis développeur <WordStyled>FrontEnd</WordStyled> passionné par le
        développement informatique, spécialisé dans le développement Front
        Javascript, je me suis orienté vers le{" "}
        <WordStyled>framework ReactJs</WordStyled> N'hésitez pas à me contacter
        pour discuter de <WordStyled>vos projets</WordStyled> !
      </MainDescription>
      <Title id="experience">EXPÉRIENCES</Title>
      <Rotate top right>
        <ContenairExperience>
          <CardExperience
            workDate={EXPERIENCE[1]?.workDate}
            employer={EXPERIENCE[1]?.employer}
            job={EXPERIENCE[1]?.job}
            workDescription={EXPERIENCE[1]?.workDescription}
            skill={EXPERIENCE[1]?.skill}
          />
        </ContenairExperience>
      </Rotate>
      <Rotate top left>
        <ContenairExperience>
          <CardExperience
            image={<img src={EXPERIENCE[0]?.image} alt="" height={120} />}
            image2={<img src={EXPERIENCE[0]?.image2} alt="" height={120} />}
            workDate={EXPERIENCE[0]?.workDate}
            employer={EXPERIENCE[0]?.employer}
            job={EXPERIENCE[0]?.job}
            workDescription={EXPERIENCE[0]?.workDescription}
            skill={EXPERIENCE[0]?.skill}
          />
        </ContenairExperience>
      </Rotate>
      <MyProjects />
      <Footer />
    </Contenair>
  );
};

export default MyDescription;
