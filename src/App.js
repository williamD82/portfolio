import "./App.css";
import { ThemeProvider } from "styled-components";
import { Theme } from "./styles/theme";
import Router from "./routes";
function App() {
  return (
    <ThemeProvider theme={Theme}>
      <Router />
    </ThemeProvider>
  );
}

export default App;
