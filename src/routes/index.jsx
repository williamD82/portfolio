import { BrowserRouter, Route, Routes } from "react-router-dom";
import Portfolio from "../components/Portfolio";

const Router = () => {
  return (
    <div>
      <BrowserRouter basename="/">
        <Routes>
          <Route path="/" element={<Portfolio />} />
          <Route path="/portfolio" element={<Portfolio />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
};

export default Router;
