import React from "react";
import { Theme } from "../../styles/theme";
import { styled } from "styled-components";
const Contenair = styled.div`
  color: ${Theme.colors.primary.lighter};
  line-height: 1.5;
  max-width: 35rem;
  margin-top: 3.2rem;
  padding: 2px;
  text-align: center;
`;
const Title = styled.p``;
const Footer = () => {
  return (
    <Contenair>
      <Title>
        © 2023 William Debezis | Dernière mise à jour : Juillet 2024
      </Title>
    </Contenair>
  );
};

export default Footer;
