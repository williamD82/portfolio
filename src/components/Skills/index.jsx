import React from "react";
import { styled } from "styled-components";
const Button = styled.button`
  color: #52c9bb;
  line-height: 1.25rem;
  font-size: 0.75rem;
  padding-top: 0.25rem;
  padding-bottom: 0.25rem;
  padding-left: 0.75rem;
  padding-right: 0.75rem;
  background-color: rgba(45, 212, 191, 0.1);
  border-radius: 9999px;
  align-items: center;
  border: none;
  margin-right: 5px;
  margin-bottom: 5px;
`;
const Skills = ({ skill }) => {
  return <Button>{skill}</Button>;
};

export default Skills;
