import React from "react";
import { Theme } from "../../styles/theme";
import { styled } from "styled-components";
import Skills from "../Skills";

const Contenair = styled.div`
  padding: 1rem;
  border-radius: 0.375rem;
  display: block;
  &:hover {
    background-color: ${Theme.colors.primary.hover};
    border: 0 solid #e5e7eb;
  }
`;
const ContenairImg = styled.div``;
const ProjetDescription = styled.p`
  color: ${Theme.colors.primary.default};
  line-height: 1.5;
  max-width: 35rem;
  font-size: 0.875rem;
  a {
    color: ${Theme.colors.primary.default};
    font-weight: bold;
    text-decoration: none;
  }

  a:hover {
    text-decoration: underline;
  }
`;

const ProjetTitle = styled.h3`
  color: ${Theme.colors.primary.lighter};
  font-size: 1rem;
`;
const CardProjet = ({ image, name, description, skill }) => {
  return (
    <Contenair>
      <ContenairImg>{image}</ContenairImg>
      <ProjetTitle>{name}</ProjetTitle>
      <ProjetDescription dangerouslySetInnerHTML={{ __html: description }} />
      {skill.map((s, index) => (
        <Skills key={index} skill={s} />
      ))}
    </Contenair>
  );
};

export default CardProjet;
