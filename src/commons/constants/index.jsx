import portfolioImg from "../../assets/images/portfolioImg.png";
import easyMenu_1 from "../../assets/images/easyMenu_1.png";
import easyMenu_2 from "../../assets/images/easyMenu_2.png";
import creativeconnect_1 from "../../assets/images/creativeconnect_1.png";
import debezis_carrelage from "../../assets/images/debezis_carrelage.png";
export const EXPERIENCE = [
  {
    image: `${easyMenu_1}`,
    image2: `${easyMenu_2}`,
    workDate: "Sept 2022 - Juin 2024",
    employer: "EasyMenu",
    job: "Développeur React.js",
    workDescription:
      "Je conçois et développe actuellement une application Web de partage de recettes, collaborant avec une équipe. J'utilise ReactJS et Typescript pour des interfaces modernes, optimise les performances avec Webpack et Babel, gére le code via Git/Github, assure la qualité avec des tests (Jest), et contribue aux revues de code. Mon approche suit une méthodologie Agile Scrum avec Jira pour la gestion des tâches.",
    skill: [
      "REACT.JS",
      "REACT-ICONS",
      "REACT-REVEAL",
      "API REST",
      "TYPESCRIPT",
      "JIRA",
      "GIT",
      "JEST",
      "REACT-HOOK-FORM",
      "STYLED-COMPONENTS",
      "ZUSTAND",
    ],
  },
  {
    workDate: "Octobre 2023 - Aujourd'hui",
    employer: "Free-lance",
    job: "Développeur React.js",
    workDescription:
      "Je vous propose des prestations de création de sites web sur mesure et de développement de logiciels spécifiques. Développement 100% sur mesure & Développement de haute qualité sont mes engagements afin de répondre précisément à vos besoins.Passionné par le développement web, mes langages préférées sont Javascript (react.js ...) Si vous souhaitez me parler d’un projet web, merci de me contacter.",
    skill: [
      "REACT.JS",
      "API REST",
      "TYPESCRIPT",
      "NODE.JS",
      "NEXT.JS",
      "GIT",
      "VSC",
      "IA",
    ],
  },
];
export const PROJET = [
  {
    image: `${portfolioImg}`,
    name: "PORTFOLIO",
    description:
      "Mon portfolio est une vitrine numérique de mon travail et de mes réalisations. Découvrez mes compétences, projets et expériences professionnels dans un format interactif et convivial.",
    skill: [
      "REACT.JS",
      "STYLED-COMPONENTS",
      "REACT-ICONS",
      "REACT-REVEAL",
      "REACT-ROUTER-DOM",
    ],
  },
  {
    image: `${creativeconnect_1}`,
    name: "CRÉATIVE CONNECT",
    description:
      "Creative Connect est une plateforme sociale dédiée aux créatifs de tous horizons.Notre plateforme vise à connecter des créateurs de divers domaines tels que les artistes, écrivains, développeurs, musiciens, et bien d'autres encore, pour collaborer sur des projets créatifs uniques. Rejoignez-nous dès aujourd'hui et transformez vos rêves créatifs en réalité grâce à Creative Connect !",
    skill: [
      "REACT.JS",
      "API REST",
      "JEST",
      "REACT-HOOK-FORM",
      "STYLED-COMPONENTS",
      "REACT-TOASTIFY",
      "REACT-ROUTER-DOM",
      "AXIOS",
      "BDD AIRTABLE",
    ],
  },
  {
    image: `${debezis_carrelage}`,
    name: "DEBEZIS CARRELAGE",
    description:
      "Debezis Carrelage est une entreprise spécialisée dans les services de carrelage et mosaïque. Nous vous offrons une solution complète pour la pose et la rénovation de vos sols et murs. Que ce soit pour votre maison, votre bureau ou un projet commercial, notre expertise garantit des résultats impeccables. <a href='https://debezis-carrelage.netlify.app/' target='_blank' rel='noopener noreferrer'>Découvrez le site</a>",
    skill: [
      "REACT.JS",
      "REACT-HOOK-FORM",
      "EMAILJS-COM",
      "REACT-ICONS",
      "REACT-SCRIPT",
      "STYLED-COMPONENTS",
      "REACT-TOASTIFY",
      "REACT-ROUTER-DOM",
      "YARN",
    ],
  },
];
