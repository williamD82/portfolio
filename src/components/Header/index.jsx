import React from "react";
import { Theme } from "../../styles/theme";
import { styled } from "styled-components";
import { AiFillGitlab, AiFillLinkedin } from "react-icons/ai";
import { TbHexagonLetterM } from "react-icons/tb";

import Slide from "react-reveal/Fade";

const Contenair = styled.div`
  padding-bottom: 6rem;
  max-height: 80vh;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  position: sticky;
  top: 6rem;
  @media (max-width: 768px) {
    position: static;
    padding: 20px;
  }
`;
const ContenairMainTitleSubTitleDescriptionListeDescription = styled.div``;
const MainTitle = styled.h1`
  color: ${Theme.colors.primary.lighter};
  font-size: ${Theme.fontSize.h1};
  letter-spacing: -0.025em;
  line-height: 1;
  cursor: pointer;
`;
const SubTitle = styled.h2`
  color: ${Theme.colors.primary.lighter};
  font-size: ${Theme.fontSize.h2};
  letter-spacing: -0.025em;
  line-height: 1.75rem;
`;
const Description = styled.p`
  color: ${Theme.colors.primary.default};
  line-height: 1.5;
  max-width: 20rem;
  margin-top: 1rem;
`;
const ListeDescription = styled.li`
  color: ${Theme.colors.primary.default};
  padding-top: 0.75rem;
  padding-bottom: 0.75rem;
  letter-spacing: 0.1em;
  cursor: pointer;
  font-size: ${Theme.fontSize.li};
  line-height: 1rem;
  &:hover {
    color: ${Theme.colors.primary.lighter};
    margin-left: 30px;
  }
`;
const UlNavigation = styled.ul`
  margin-top: 4rem;
  list-style: none;
  margin: 0px;
  padding: 0px;
`;
const UlSocial = styled.a`
  display: flex;
  list-style: none;
  margin-left: 0.25rem;
  margin: 0px;
  padding: 0px;
  a {
    color: ${Theme.colors.primary.lighter};
    &:visited {
      color: ${Theme.colors.primary.lighter};
    }
    &:hover {
      color: ${Theme.colors.primary.default};
    }
    &:active {
      color: ${Theme.colors.primary.lighter};
    }
  }
`;
const ListeSocial = styled.li`
  color: ${Theme.colors.primary.lighter};
  padding-top: 0.75rem;
  padding-bottom: 0.75rem;
  letter-spacing: 0.1em;
  cursor: pointer;
  font-size: 50px;
  line-height: 1rem;
  margin-left: 20px;
  &:hover {
    color: ${Theme.colors.primary.default};
  }
`;
const LinksDescription = styled.a`
  text-decoration: none;
  outline: none;
  color: ${Theme.colors.primary.lighter};
  &:active,
  &:visited {
    color: ${Theme.colors.primary.lighter};
  }
`;

const Header = () => {
  return (
    <Contenair>
      <Slide top>
        <ContenairMainTitleSubTitleDescriptionListeDescription>
          <MainTitle>William Debezis</MainTitle>
          <SubTitle>Développeur REACT. JS / NODE. JS</SubTitle>
          <Description>
            Je conçois des produits et des expériences numériques sur le Web, en
            veillant à ce qu'ils soient accessibles et inclusifs pour tous.
          </Description>
          <nav>
            <UlNavigation>
              <ListeDescription>
                <LinksDescription href="#description">
                  À PROPOS
                </LinksDescription>
              </ListeDescription>
              <ListeDescription>
                <LinksDescription href="#experience">
                  EXPÉRIENCE
                </LinksDescription>
              </ListeDescription>
              <ListeDescription>
                <LinksDescription href="#projet">PROJETS</LinksDescription>
              </ListeDescription>
            </UlNavigation>
          </nav>
        </ContenairMainTitleSubTitleDescriptionListeDescription>
      </Slide>
      <UlSocial>
        <Slide left>
          <ListeSocial>
            <a
              href="https://gitlab.com/username"
              target="_blank"
              rel="noopener noreferrer"
            >
              <AiFillGitlab size={"2.5rem"} />
            </a>
          </ListeSocial>
          <ListeSocial>
            <a
              href="https://www.linkedin.com/in/william-debézis-b25106267"
              target="_blank"
              rel="noopener noreferrer"
            >
              <AiFillLinkedin size={"2.5rem"} />
            </a>
          </ListeSocial>
          <ListeSocial>
            <a
              href="https://www.malt.fr/profile/willliamdebezis1?overview"
              target="_blank"
              rel="noopener noreferrer"
            >
              <TbHexagonLetterM size={"2.5rem"} />
            </a>
          </ListeSocial>
        </Slide>
      </UlSocial>
    </Contenair>
  );
};

export default Header;
