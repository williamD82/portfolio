import { styled } from "styled-components";
import Theme from "../../styles/theme";
import Header from "../Header";
import MyDescription from "../MyDescription";
const Contenair = styled.div`
  display: flex;
  justify-content: space-evenly;
  background-color: ${Theme.colors.background.default};
  font-family: Roboto;
  @media (max-width: 768px) {
    flex-direction: column;
  }
`;
const Portfolio = () => {
  return (
    <Contenair>
      <Header />
      <MyDescription />
    </Contenair>
  );
};

export default Portfolio;
