import React from "react";
import { Theme } from "../../styles/theme";
import { styled } from "styled-components";
import Skills from "../Skills";
const Contenair = styled.div`
  padding: 1rem;
  border-radius: 0.375rem;
  display: block;
  &:hover {
    background-color: ${Theme.colors.primary.hover};
    border: 0 solid #e5e7eb;
    color: #52c9bb;
  }
`;
const Header = styled.div``;
const WorkDescription = styled.p`
  color: ${Theme.colors.primary.default};
  line-height: 1.5;
  max-width: 35rem;
  font-size: 0.875rem;
`;
const ContenairH3 = styled.div`
  font-size: 0.875rem;
`;
const WorkTitle = styled.div`
  color: ${Theme.colors.primary.lighter};
  font-size: 1rem;
`;
const ContenairImg = styled.div`
  display: flex;
  align-items: center;
  flex-wrap: wrap;
  justify-content: space-between;
  @media (max-width: 768px) {
    display: flex;
    justify-content: center;
  }
`;
const Img1 = styled.div`
  @media (max-width: 768px) {
    margin-bottom: 20px;
  }
`;

const CardExperience = ({
  workDate,
  employer,
  job,
  skill,
  workDescription,
  image,
  image2,
}) => {
  return (
    <Contenair>
      <Header>
        {workDate}. {employer}
      </Header>
      <ContenairH3>
        <WorkTitle>{job}</WorkTitle>
        <ContenairImg>
          <Img1>{image}</Img1>
          {image2}
        </ContenairImg>
      </ContenairH3>
      <WorkDescription>{workDescription}</WorkDescription>
      {skill.map((s, index) => (
        <Skills key={index} skill={s} />
      ))}
    </Contenair>
  );
};

export default CardExperience;
