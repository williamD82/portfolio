const colors = {
  white: "#FFFFFF",
  red: "#F24750",
  green: "#1BC773",
  black: "black",
  primary: {
    lighter: "#EEF6FA",
    hover: "rgba(30, 41, 59, 0.5)",
    default: "#94A3B8",
    dark: "#004269",
  },
  secondary: {
    lighter: "#EEF6FA",
    light: "#819CAD",
    default: "#3A3A3A",
    dark: "#004269",
  },
  boxShadow: { default: "0 0 10px 1px #52c9bb" },
  background: { default: "#10172A" },
};
const fontSize = {
  small: "11px",
  regular: "14px",
  xlarge: "20px",
  h1: "3rem",
  h2: "1,25rem",
  h3: "1 rem",
  p: "0,75 rem",
  li: "0.75rem",
};
const padding = {
  regular: "20px",
};
export const Theme = {
  colors,
  fontSize,
  padding,
  innerSize: 1170,
  background: colors.white,
  text: colors.black,
  success: colors.green,
  danger: colors.red,
};
export default Theme;
