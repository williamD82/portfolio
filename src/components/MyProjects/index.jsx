import React from "react";
import { PROJET } from "../../commons/constants";
import { Theme } from "../../styles/theme";
import { styled } from "styled-components";
import CardProjet from "../CardProjet";
import { Slide } from "react-reveal";
const Contenair = styled.div``;
const ContenairProjet = styled.div`
  color: ${Theme.colors.primary.default};
  line-height: 1.5;
  max-width: 35rem;
  margin-top: 1.2rem;
  padding: 2px;
`;
const Title = styled.h3`
  margin-top: 50px;
  color: ${Theme.colors.primary.default};
`;
const MyProjects = () => {
  console.log(PROJET);
  return (
    <Contenair>
      <Title id="projet">PROJETS</Title>
      <Slide right>
        <ContenairProjet>
          {PROJET?.map((p) => (
            <CardProjet
              image={<img src={p?.image} alt="" height={120} />}
              name={p?.name}
              description={p?.description}
              skill={p?.skill}
            />
          ))}
        </ContenairProjet>
      </Slide>
    </Contenair>
  );
};

export default MyProjects;
